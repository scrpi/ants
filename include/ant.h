#ifndef ANT_H_
#define ANT_H_

#include <cstdlib>
#include <iostream>
#include <cmath>
#include "app.h"

class App;

const int ANT_SPEED = 30;
const int ANT_VISION = 20;
const int HEADING_BUFFER = 5;

//==============================================================================
class Ant 
{
private:
    App*            	mpApp;
    float           	mPX;
    float           	mPY;
    float          		mHeading;
    SDL_Texture*		mpTexture;

public:
    Ant(App*, int, int);
    void            	Update(float);
    void            	Render();

};


#endif // ANT_H_
