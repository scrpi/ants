#ifndef APP_H_
#define APP_H_

#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <time.h>
#include <list>

#include "ant.h"
#include "anthill.h"

class Ant;
class AntHill;

//Screen dimension constants
const int ANTS_SCREEN_W = 640;
const int ANTS_SCREEN_H = 640;

const double PI = 3.1415926535897;

const int kNumAnts = 100;

//==============================================================================
class App 
{
private:
    SDL_Window*         mpWindow;
    bool                mRunning;
    Uint32              mLastTicks;
    std::list<Ant>      mAnts;
    AntHill*            mpAntHill;

    int                 Init();
    void                Update(float);
    void                Render();
    float               Clock(Uint32);
    void                Loop();

public:
    SDL_Renderer*       mpRenderer;
    
    App();
    void                SpawnAnt(int, int);
    void                KillAnt();
    int                 GetNumAnts();
    int                 Run();
    void                Event(SDL_Event*);
    float               GetRandF();
    void                Cleanup();
};


#endif // APP_H_
