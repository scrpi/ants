#ifndef ANTHILL_H_
#define ANTHILL_H_

#include "app.h"

class App;

const int kSpawnTime = 250; // time in ms for an ant to spawn

//==============================================================================
class AntHill
{
private:
	App*				mpApp;
	int					mPX;
	int					mPY;
	int					mMaxAnts;
	int					mNumAnts;
	float				mTimeSinceLastSpawn;

public:
	AntHill(App*, int, int, int);
	void 				Update(float);
};

#endif // ANTHILL_H_
