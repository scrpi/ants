#include "ant.h"

Ant::Ant(App* app, int x, int y)
{
    mpApp         = app;
    mPX           = (float)x;
    mPY           = (float)y;
    
    // Calculate initial heading
    mHeading      = 2 * PI * mpApp->GetRandF();

    SDL_Surface* s = IMG_Load("ant.png");
    if (s == NULL) {
        std::cout << "IMG_Load Failed: " << SDL_GetError() << std::endl;
    }
    mpTexture = SDL_CreateTextureFromSurface(mpApp->mpRenderer, s);
    SDL_FreeSurface(s);
    if (mpTexture == NULL) {
        std::cout << "SDL_CreateTextureFromSurface Failed: " << SDL_GetError() << std::endl;
    }
}

void Ant::Update(float dt)
{
    // Calculate new heading
    mHeading += (mpApp->GetRandF() - 0.5) * (ANT_SPEED / 200.0);
    
    // Clamp heading
    if (mHeading < 0) mHeading = 2 * PI + mHeading;
    if (mHeading >= 2 * PI) mHeading = 0 + (mHeading - 2 * PI);

    // Calculate new position
    float dx = std::sin(mHeading) * ANT_SPEED * dt;
    float dy = std::cos(mHeading) * ANT_SPEED * dt;

    // Clamp position
    bool hit = false;
    if (ANTS_SCREEN_W < mPX + dx || mPX + dx < 0) {
        // We hit the right or left side
        mHeading = 2 * PI - mHeading;
        hit = true;
    }
    if (ANTS_SCREEN_H < mPY + dy || mPY + dy < 0) {
        // We hit the top or bottom
        mHeading = 2 *  PI - (mHeading - PI);
        hit = true;
    }
    if (!hit) {
        mPX += dx;
        mPY += dy;
    }
}

void Ant::Render()
{
    // Draw our ant
    SDL_Rect dest;
    dest.x = (int)mPX - 2;
    dest.y = (int)mPY - 3;
    dest.w = 5;
    dest.h = 7;

    int deg = 360 - (int)(mHeading * 57.2957795);

    SDL_RenderCopyEx(mpApp->mpRenderer, mpTexture, NULL, &dest, deg, NULL, SDL_FLIP_VERTICAL);

    // Draw field of vision circle
    int x = ANT_VISION;
    int y = 0;
    int radiusError = 1 - ANT_VISION;

    SDL_SetRenderDrawColor(mpApp->mpRenderer, 77, 103, 179, 255);

    while (x >= y) {
        SDL_RenderDrawPoint(mpApp->mpRenderer,  x + mPX,  y + mPY);
        SDL_RenderDrawPoint(mpApp->mpRenderer,  y + mPX,  x + mPY);
        SDL_RenderDrawPoint(mpApp->mpRenderer, -x + mPX,  y + mPY);
        SDL_RenderDrawPoint(mpApp->mpRenderer, -y + mPX,  x + mPY);
        SDL_RenderDrawPoint(mpApp->mpRenderer, -x + mPX, -y + mPY);
        SDL_RenderDrawPoint(mpApp->mpRenderer, -y + mPX, -x + mPY);
        SDL_RenderDrawPoint(mpApp->mpRenderer,  x + mPX, -y + mPY);
        SDL_RenderDrawPoint(mpApp->mpRenderer,  y + mPX, -x + mPY);
        y++;
        if (radiusError < 0) {
            radiusError += 2 * y + 1;
        } else {
            x--;
            radiusError += 2 * (y - x + 1);
        }
    }
}
