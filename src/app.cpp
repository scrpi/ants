#include "app.h"

const Uint32 kFPS = 60;

//==============================================================================
App::App()
{
    mpWindow        = NULL;
    mRunning        = true;
}

//==============================================================================
int App::Init()
{
    if(SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
        return -1;
    }

    mpWindow = SDL_CreateWindow(
        "SDL Tutorial",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        ANTS_SCREEN_W, ANTS_SCREEN_H, SDL_WINDOW_SHOWN
    );
    if(mpWindow == NULL)
    {
        printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
        return -1;
    }

    // Show render drivers
    /*
    SDL_RendererInfo* info;
    for (int i = 0; i < SDL_GetNumRenderDrivers(); i++) {
        SDL_GetRenderDriverInfo(i, info);
        std::cout << "Render Driver " << i << ":" << info->name << std::endl;
    }
    */

    mpRenderer = SDL_CreateRenderer(mpWindow, -1, SDL_RENDERER_ACCELERATED);
    
    SDL_SetRenderDrawBlendMode(mpRenderer, SDL_BLENDMODE_BLEND);

    IMG_Init(IMG_INIT_PNG);

    return 0;
}

//==============================================================================
void App::Event(SDL_Event* event) 
{
    if (event->type == SDL_QUIT) {
        mRunning = false;
        return;
    }
    if (event->type == SDL_KEYDOWN) {
        switch (event->key.keysym.sym) {
            case SDLK_ESCAPE:
                mRunning = false;
                return;
        }
    }
}

//==============================================================================
void App::Update(float dt)
{
    for (std::list<Ant>::iterator ant = mAnts.begin(); ant != mAnts.end(); ant++) {
        ant->Update(dt);
    }

    mpAntHill->Update(dt);
}

//==============================================================================
void App::Render()
{
    // Fill background
    SDL_SetRenderDrawColor(mpRenderer, 200, 200, 200, SDL_ALPHA_OPAQUE);
    SDL_RenderFillRect(mpRenderer, NULL);
    
    for (std::list<Ant>::iterator ant = mAnts.begin(); ant != mAnts.end(); ant++) {
        ant->Render();
    }

    SDL_RenderPresent(mpRenderer);
}

//==============================================================================
float App::Clock(Uint32 fps)
{
    int delay = (1000 / fps) - (SDL_GetTicks() - mLastTicks);
    if (delay < 0) delay = 0;
    SDL_Delay(delay);
    Uint32 before = mLastTicks;
    Uint32 now = SDL_GetTicks();
    mLastTicks = now;
    return (now - before) / 1000.0;
}

//==============================================================================
void App::Loop()
{
    float dt;
    int startTicks, frameTime;
    std::string title;
    SDL_Event event;
    while (mRunning) {
        dt = Clock(kFPS);

        startTicks = SDL_GetTicks();

        // Handle Events
        while(SDL_PollEvent(&event) != 0) Event(&event);

        // Update state
        Update(dt);

        // Render to screen
        Render();

        frameTime = SDL_GetTicks() - startTicks;
        title = std::to_string(frameTime) + "ms per frame";
        SDL_SetWindowTitle(mpWindow, title.c_str());
    }
}

//==============================================================================
void App::SpawnAnt(int px, int py)
{
    mAnts.push_front(Ant(this, px, py));
}

//==============================================================================
void App::KillAnt()
{
    mAnts.pop_back();
}

//==============================================================================
int App::GetNumAnts()
{
    return mAnts.size();
}

//==============================================================================
int App::Run()
{
    if (Init() < 0) {
        printf("INIT ERROR!\n");
        return -1;
    }

    // Create an Ant Hill
    mpAntHill = new AntHill(this, ANTS_SCREEN_W/2, ANTS_SCREEN_H/2, kNumAnts);

    Loop();
    Cleanup();
}

//==============================================================================
float App::GetRandF()
{
    return (float)rand() / RAND_MAX;
}

//==============================================================================
void App::Cleanup() 
{
	IMG_Quit();
    SDL_DestroyWindow( mpWindow );
    SDL_Quit();
}
