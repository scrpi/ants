#include "anthill.h"

AntHill::AntHill(App* app, int px, int py, int maxAnts)
{
	mpApp = app;
	mPX = px;
	mPY = py;
	mMaxAnts = maxAnts;
	mNumAnts = 0;
	mTimeSinceLastSpawn = kSpawnTime;
}

void AntHill::Update(float dt)
{
	mTimeSinceLastSpawn += dt * 1000;
	if (mTimeSinceLastSpawn > kSpawnTime) {
		if (mNumAnts < mMaxAnts) {
			mpApp->SpawnAnt(mPX, mPY);
			mNumAnts = mpApp->GetNumAnts();
			mTimeSinceLastSpawn -= kSpawnTime;
		} else {
			mTimeSinceLastSpawn = kSpawnTime;
		}
	}
}